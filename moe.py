"""Mixture-of-Experts components from `tensor2tensor.utils.expert_utils` wrapped into classes."""

import tensorflow as tf


class SparseDispatcher:

  def __init__(self, num_experts, gates):
    self._gates = gates
    self._num_experts = num_experts

    where = tf.compat.v1.to_int32(tf.where(tf.transpose(gates) > 0))

    self._expert_index, self._batch_index = tf.unstack(where, num=2, axis=1)
    self._part_sizes_tensor = tf.reduce_sum(tf.compat.v1.to_int32(gates > 0), [0])
    self._nonzero_gates = tf.gather(
        tf.reshape(self._gates, [-1]),
        self._batch_index * num_experts + self._expert_index)

  def dispatch(self, inp):
    inp = tf.gather(inp, self._batch_index)
    return tf.split(inp, self._part_sizes_tensor, 0, num=self._num_experts)

  def combine(self, expert_out):
    stitched = tf.concat(expert_out, 0)
    stitched *= tf.expand_dims(self._nonzero_gates, 1)
    combined = tf.compat.v1.unsorted_segment_sum(
        stitched, self._batch_index, tf.shape(self._gates)[0])

    return combined

  def expert_to_gates(self):
    return tf.split(self._nonzero_gates, self._part_sizes_tensor, 0, num=self._num_experts)

  def expert_to_batch_indices(self):
    return tf.split(self._batch_index, self._part_sizes_tensor, 0, num=self._num_experts)
