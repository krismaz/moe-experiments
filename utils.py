"""Utility functions from `tensor2tensor.utils.expert_utils` adapted to TF 2."""

import math
import tensorflow as tf


def cv_squared(x):
  epsilon = 1e-10
  float_size = tf.compat.v1.to_float(tf.size(x)) + epsilon

  mean = tf.reduce_sum(x) / float_size
  variance = tf.reduce_sum(
      tf.compat.v1.squared_difference(x, mean)) / float_size

  return variance / (tf.square(mean) + epsilon)


def normal_distribution_cdf(x, stddev):
  return 0.5 * (1.0 + tf.math.erf(x / (math.sqrt(2) * stddev + 1e-20)))


def prob_in_top_k(clean_values, noisy_values, noise_stddev, noisy_top_values, k):
  batch = tf.shape(clean_values)[0]
  m = tf.shape(noisy_top_values)[1]
  top_values_flat = tf.reshape(noisy_top_values, [-1])

  threshold_positions_if_in = tf.range(batch) * m + k
  threshold_if_in = tf.expand_dims(
      tf.gather(top_values_flat, threshold_positions_if_in), 1)
  is_in = tf.greater(noisy_values, threshold_if_in)

  if noise_stddev is None:
    return tf.to_float(is_in)

  threshold_positions_if_out = threshold_positions_if_in - 1
  threshold_if_out = tf.expand_dims(
      tf.gather(top_values_flat, threshold_positions_if_out), 1)

  prob_if_in = normal_distribution_cdf(
      clean_values - threshold_if_in, noise_stddev)
  prob_if_out = normal_distribution_cdf(
      clean_values - threshold_if_out, noise_stddev)
  prob = tf.where(is_in, prob_if_in, prob_if_out)

  return prob


def rowwise_unsorted_segment_sum(values, indices, n):
  batch, k = tf.unstack(tf.shape(indices), num=2)

  indices_flat = tf.reshape(indices, [-1]) + n * tf.compat.v1.div(
      tf.range(batch * k), k)

  ret_flat = tf.compat.v1.unsorted_segment_sum(
      tf.reshape(values, [-1]), indices_flat, batch * n)

  return tf.reshape(ret_flat, [batch, n])


def gates_to_load(gates):
  return tf.reduce_sum(tf.compat.v1.to_float(gates > 0), 0)

